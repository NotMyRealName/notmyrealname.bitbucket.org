
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var imena = [["Franc", "Janez", "Ivan", "Anton", "Marko"], ["Marija", "Ana", "Maja", "Irena", "Mojca"]];
var priimki = ["Novak", "Horvat", "Kovačič", "Krajnc", "Zupančič", "Potočnik", "Kovač", "Mlakar", "Kos", "Vidmar"];

var uporabniki = [
        "d7b5eebc-2ee0-49d8-83fb-17928b11be3c",
        "31feef8b-8cf5-46b3-b654-d1be8ed935bc",
        "ce63161a-a3cb-42ac-852e-3974a677d99d"
    ];

function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

//d7b5eebc-2ee0-49d8-83fb-17928b11be3c

//31feef8b-8cf5-46b3-b654-d1be8ed935bc

//ce63161a-a3cb-42ac-852e-3974a677d99d


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(generirajVse) {
    
    var sessionId = getSessionId();

    console.log("sessionID: "+sessionId);

    var spol = getRndInteger(0,1);
    var ime = imena[spol][getRndInteger(0, imena[spol].length -1)];
    var priimek = priimki[getRndInteger(0, priimki[spol].length -1)];
    var date = randomDate(new Date(1900, 0, 1), new Date());
    var datumRojstva = date.toISOString();

	$.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	$.ajax({
	    url: baseUrl + "/ehr",
		type: 'POST',
		success: function (data) {
		    console.log("success 1");
		    var ehrId = data.ehrId;
		    var partyData = {
		        firstNames: ime,
		        lastNames: priimek,
		        dateOfBirth: datumRojstva,
		        partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
            };
		    $.ajax({
		        url: baseUrl + "/demographics/party",
		        type: 'POST',
	            contentType: 'application/json',
	            data: JSON.stringify(partyData),
	            success: function (party) {
	                console.log("success 2");
	                if (party.action == 'CREATE') {
	                    alert("Uspesno dodan uporabnik z EHRid: "+ ehrId);
	                    if(generirajVse){
                    	    for(var i=0;i<10;i++){
                    	        dodajMeritveVitalnihZnakov(sessionId, ehrId);
                                //await sleep(2000);
                    	    }
                    	}
	                }
	            },
	            error: function(err) {
    		    	console.log(JSON.parse(err.responseText).userMessage);
    		    }
	        });
	    },
        error: function(err) {
	    	console.log(JSON.parse(err.responseText).userMessage);
	    }
	});
}

/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov(sessionId, ehrId) {

	var datumInUra = randomDate(new Date(2016, 0, 1), new Date()).toISOString();
	var telesnaVisina = getRndInteger(80, 210);
	var telesnaTeza = getRndInteger(30, 100);
	var telesnaTemperatura = getRndInteger(33, 43);
	var sistolicniKrvniTlak = getRndInteger(80, 170);
	var diastolicniKrvniTlak = getRndInteger(30, 120);
	var nasicenostKrviSKisikom = getRndInteger(90, 100);
	var merilec = "dr. "+imena[getRndInteger(0, 1)][getRndInteger(0, 4)];

	$.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	var podatki = {
		// Struktura predloge je na voljo na naslednjem spletnem naslovu:
        // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
	    "ctx/language": "en",
	    "ctx/territory": "SI",
	    "ctx/time": datumInUra,
	    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
	    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
	   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
	    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
	    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
	    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
	    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
	};
	var parametriZahteve = {
	    ehrId: ehrId,
	    templateId: 'Vital Signs',
	    format: 'FLAT',
	    committer: merilec
	};
	$.ajax({
	    url: baseUrl + "/composition?" + $.param(parametriZahteve),
	    type: 'POST',
	    contentType: 'application/json',
	    data: JSON.stringify(podatki),
	    success: function (res) {
	        console.log("dodajanje meritev OK");
	    }
	});
}

/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika(ehrId) {
	var sessionId = getSessionId();

	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
    	success: function (data) {
			var party = data.party;
			$("#userList").append('<option value="'+ehrId+'">'+party.firstNames+' '+party.lastNames+', '+party.dateOfBirth+'</option>');
		},
		error: function(err) {
			console.log(JSON.parse(err.responseText).userMessage);
		}
	});
}

function preberiMeritveVitalnihZnakov(sessionId, ehrId, tip) {
	$.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    	type: 'GET',
    	headers: {"Ehr-Session": sessionId},
    	success: function (data) {
			if (tip == "temperature") {
				$.ajax({
  				    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
					        for (var i in res) {
					            $("#dataTemperature").append("<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].temperature +
                      " " + res[i].unit + "</td></tr>");
					        }
				    	} else {
				    	    $("#dataTemperature").append("<tr><td> Ni podatkov! </td></tr>");
				    	}
				    },
				    error: function(err) {
				    	console.log(JSON.parse(err.responseText).userMessage);
				    }
				});
			} else if (tip == "weight") {
				$.ajax({
				    url: baseUrl + "/view/" + ehrId + "/" + "weight",
				    type: 'GET',
				    headers: {"Ehr-Session": sessionId},
				    success: function (res) {
				    	if (res.length > 0) {
					        for (var i in res) {
					            $("#dataWeight").append("<tr><td>" + res[i].time +
                      "</td><td class='text-right'>" + res[i].weight +
                      " " + res[i].unit + "</td></tr>");
					        }
				    	} else {
				    		$("#dataTemperature").append("<tr><td> Ni podatkov! </td></tr>");
				    	}
				    },
				    error: function(err) {
				    	console.log(JSON.parse(err.responseText).userMessage);
				    }
				});
			}
    	},
    	error: function(err) {
    		console.log(JSON.parse(err.responseText).userMessage);
    	}
	});
}

function displayUserData(ehrId){
    console.log(ehrId);
    var sessionId = getSessionId();
    $.ajax({
		url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		type: 'GET',
		headers: {"Ehr-Session": sessionId},
    	success: function (data) {
			var party = data.party;
			$("#userDataName").html(party.firstNames);
			$("#userDataSurname").html(party.lastNames);
			$("#userDataBirthDate").html(party.dateOfBirth);
		},
		error: function(err) {
			console.log(JSON.parse(err.responseText).userMessage);
		}
	});
	
	var tipiPodatkov = ["temperature", "weight"];
	var len = tipiPodatkov.length -1;
	for(var i = len; i>=0;i--){
	    preberiMeritveVitalnihZnakov(sessionId, ehrId, tipiPodatkov[i]);
	}
	
	console.log("getting called");
	
	$("#userSelectionRow").addClass("hidden");
	$("#userDataRow").removeClass("hidden");
}

$("document").ready(function(){
    $("#generiraj").click(function(){
        console.log("generating...");
        generirajPodatke(false);
    });
    var len = uporabniki.length -1;
    for(var i=len;i>=0;i--){
        preberiEHRodBolnika(uporabniki[i]);
    }
    $("#potrdi1").click(function(){displayUserData($("#ehridInput").val());});
    $("#potrdi2").click(function(){displayUserData($("#userList").val());});
});


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija


/*global $*/